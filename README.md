# Reactive banking application  
This is an async RxJava3 + Jersey server, tested with Cucumber functional tests.  
Latest test report is available at project's
[GitLab Pages deployment](https://ninekflames.gitlab.io/reactive-banking-backend/).  

## How to build  
This guide assumes that you have a UNIX-like machine with **Java 11** and **Maven** installed.
If not, please look up how to install and run Java 11 and Maven on your machine (if it's possible),
then it shouldn't be hard to figure out how to change tasks below to suit **your** environment, 
if any changes are needed at all.  

0. Check out this repo.  
0. Navigate to project root folder.  
0. Run `mvn clean veriy package`.  

## How to run    

0. Locate the project jar:  
   * if built locally, is should be at `${project_dir}/target/banking-backend-${version}.jar`;
   * always available in 
 [latest job artifacts in GitLab CI](https://gitlab.com/NineKFlames/reactive-banking-backend/-/jobs/).  
0. Run `java -jar ${path_to_jar}`.  
0. Service is now available at `localhost:8888`.  
