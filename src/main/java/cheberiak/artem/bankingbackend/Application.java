package cheberiak.artem.bankingbackend;

import cheberiak.artem.bankingbackend.config.Config;
import io.netty.channel.Channel;
import org.glassfish.jersey.netty.httpserver.NettyHttpContainerProvider;
import org.glassfish.jersey.server.ResourceConfig;

import javax.ws.rs.core.UriBuilder;
import java.net.URI;
import java.util.logging.Logger;

import static org.glassfish.jersey.logging.LoggingFeature.DEFAULT_LOGGER_NAME;

public class Application {
    public static void main(String[] args) {
        URI baseUri = UriBuilder.fromUri("http://localhost/").port(8888).build();
        ResourceConfig resourceConfig = new Config();
        Channel server = NettyHttpContainerProvider.createServer(baseUri, resourceConfig, false);
        if (server.isActive()) {
            Logger.getLogger(DEFAULT_LOGGER_NAME).info(() -> "Server started successfully on " + server.localAddress());
        }
    }
}
