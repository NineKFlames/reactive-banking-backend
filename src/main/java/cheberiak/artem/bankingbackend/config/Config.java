package cheberiak.artem.bankingbackend.config;

import org.glassfish.jersey.jackson.internal.jackson.jaxrs.json.JacksonJsonProvider;
import org.glassfish.jersey.logging.LoggingFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.ServerProperties;

import javax.ws.rs.ApplicationPath;
import java.util.logging.Level;
import java.util.logging.Logger;

@ApplicationPath("/")
public class Config extends ResourceConfig {
    public Config() {
        packages("cheberiak.artem.bankingbackend");
        property(ServerProperties.BV_SEND_ERROR_IN_RESPONSE, true);
        register(JacksonJsonProvider.class);
        register(new LoggingFeature(Logger.getLogger(LoggingFeature.DEFAULT_LOGGER_NAME),
                                    Level.INFO,
                                    LoggingFeature.Verbosity.PAYLOAD_ANY,
                                    10000));
    }
}
