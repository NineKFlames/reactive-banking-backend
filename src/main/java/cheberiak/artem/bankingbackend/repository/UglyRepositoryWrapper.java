package cheberiak.artem.bankingbackend.repository;

import cheberiak.artem.bankingbackend.controller.exceptions.InvalidAmountException;
import com.intel471.task.UglyAccountRepo;
import io.reactivex.rxjava3.core.Completable;
import io.reactivex.rxjava3.core.Single;
import lombok.Synchronized;

import java.math.BigDecimal;

/**
 * Wrapper around {@link UglyAccountRepo}.
 * <p>
 * Provides synchronous and guaranteed access to the repo since the internal implementation doesn't.
 */
public class UglyRepositoryWrapper {
    private final UglyAccountRepo realRepo;

    public UglyRepositoryWrapper() {
        realRepo = new UglyAccountRepo();
    }

    private static boolean retryOnTryLaterException(Throwable e) {
        return e.getMessage().contains("Please try again later");
    }

    @Synchronized
    public void createAccountSynchronously(String accountId, Long initialBalance) {
        realRepo.createAccount(accountId, initialBalance)
                .retry(UglyRepositoryWrapper::retryOnTryLaterException)
                .blockingAwait();
    }

    @Synchronized
    public Long getBalanceSynchronously(String accountId) {
        return getBalance(accountId).blockingGet();
    }

    private Single<Long> getBalance(String accountId) {
        return realRepo.getBalance(accountId).retry(UglyRepositoryWrapper::retryOnTryLaterException);
    }

    @Synchronized
    public void depositSynchronously(String id, BigDecimal depositAmount) {
        getBalance(id).map(amount -> amount + depositAmount.multiply(BigDecimal.valueOf(100)).longValue())
                      .flatMapCompletable(amount -> setBalance(id, amount))
                      .blockingAwait();
    }

    private Completable setBalance(String accountId, Long newBalance) {
        return realRepo.setBalance(accountId, newBalance).retry(UglyRepositoryWrapper::retryOnTryLaterException);
    }

    @Synchronized
    public void withdrawSynchronously(String id, BigDecimal depositAmount) {
        getBalance(id).map(amount -> amount - depositAmount.multiply(BigDecimal.valueOf(100)).longValue())
                      .flatMapCompletable(amount -> {
                          if (amount < 0) {
                              throw new InvalidAmountException("Insufficient funds!");
                          }

                          return setBalance(id, amount);
                      })
                      .blockingAwait();
    }

    @Synchronized
    public void transferSynchronously(String target, String source, BigDecimal amount) {
        Long sourceBalance = getBalance(source).blockingGet();
        Long targetBalance = getBalance(target).blockingGet();
        long amountAsLong = amount.multiply(BigDecimal.valueOf(100)).longValue();

        if (sourceBalance - amountAsLong < 0) {
            throw new InvalidAmountException("Insufficient funds!");
        }

        setBalance(source, sourceBalance - amountAsLong).blockingAwait();
        setBalance(target, targetBalance + amountAsLong).blockingAwait();
    }
}
