package cheberiak.artem.bankingbackend.controller;

import cheberiak.artem.bankingbackend.controller.dto.request.DepositFundsRequest;
import cheberiak.artem.bankingbackend.controller.dto.request.TransferFundsRequest;
import cheberiak.artem.bankingbackend.controller.dto.request.WithdrawFundsRequest;
import cheberiak.artem.bankingbackend.controller.dto.response.AccountCreationResponse;
import cheberiak.artem.bankingbackend.controller.dto.response.BalanceResponse;
import cheberiak.artem.bankingbackend.controller.dto.response.ExceptionResponse;
import cheberiak.artem.bankingbackend.controller.exceptions.InvalidAmountException;
import cheberiak.artem.bankingbackend.service.AccountService;
import com.intel471.task.AccountNotFoundException;
import io.reactivex.rxjava3.functions.Action;
import io.reactivex.rxjava3.functions.Consumer;

import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.Response;
import java.util.function.Function;

import static java.util.concurrent.TimeUnit.SECONDS;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.Response.Status.*;

@Path("account")
public class AccountController {
    private static final int DEFAULT_TIMEOUT = 30;
    private final AccountService service;

    public AccountController() {
        service = AccountService.getInstance();
    }

    @POST
    @Path("new")
    @Produces(APPLICATION_JSON)
    public void createAccount(@Suspended AsyncResponse ar) {
        setDefaultTimeout(ar);
        service.createNewAccount()
               .subscribe(getConsumer(CREATED, AccountCreationResponse::new, ar), getDefaultExceptionHandler(ar))
               .dispose();
    }

    @GET
    @Path("{id}")
    @Produces(APPLICATION_JSON)
    public void checkBalanceForId(@Suspended AsyncResponse ar, @PathParam("id") String id) {
        setDefaultTimeout(ar);
        service.getBalance(id)
               .subscribe(getConsumer(OK, amount -> new BalanceResponse(id, amount), ar),
                          getDefaultExceptionHandler(ar))
               .dispose();
    }

    @POST
    @Path("deposit")
    @Consumes(APPLICATION_JSON)
    @Produces(APPLICATION_JSON)
    public void deposit(@Suspended AsyncResponse ar, @Valid DepositFundsRequest request) {
        setDefaultTimeout(ar);
        service.deposit(request.getTarget(), request.getAmount())
               .subscribe(getNoContentResponseAction(ar), getDefaultExceptionHandler(ar))
               .dispose();
    }

    @POST
    @Path("transfer")
    @Consumes(APPLICATION_JSON)
    @Produces(APPLICATION_JSON)
    public void transfer(@Suspended AsyncResponse ar, @Valid TransferFundsRequest request) {
        setDefaultTimeout(ar);
        service.transfer(request.getTarget(), request.getSource(), request.getAmount())
               .subscribe(getNoContentResponseAction(ar), getDefaultExceptionHandler(ar))
               .dispose();
    }

    @POST
    @Path("withdraw")
    @Consumes(APPLICATION_JSON)
    @Produces(APPLICATION_JSON)
    public void withdraw(@Suspended AsyncResponse ar, @Valid WithdrawFundsRequest request) {
        setDefaultTimeout(ar);
        service.withdraw(request.getSource(), request.getAmount())
               .subscribe(getNoContentResponseAction(ar), getDefaultExceptionHandler(ar))
               .dispose();
    }

    public static <T, R> Consumer<T> getConsumer(Response.Status status,
                                                 Function<T, R> responseBodyFunction,
                                                 AsyncResponse ar) {
        return data -> ar.resume(Response.status(status)
                                         .entity(responseBodyFunction.apply(data)).build());
    }

    private Action getNoContentResponseAction(@Suspended AsyncResponse ar) {
        return () -> ar.resume(Response.noContent().build());
    }

    private static Consumer<Throwable> getDefaultExceptionHandler(AsyncResponse ar) {
        return err -> ar.resume(
                Response.status(calculateErrorStatus(err))
                        .entity(new ExceptionResponse(err))
                        .build());
    }

    private static Response.Status calculateErrorStatus(Throwable e) {
        if (e instanceof AccountNotFoundException) {
            return NOT_FOUND;
        }

        if (e instanceof InvalidAmountException) {
            return BAD_REQUEST;
        }

        return INTERNAL_SERVER_ERROR;
    }

    private static void setDefaultTimeout(AsyncResponse ar) {
        ar.setTimeout(DEFAULT_TIMEOUT, SECONDS);
    }
}
