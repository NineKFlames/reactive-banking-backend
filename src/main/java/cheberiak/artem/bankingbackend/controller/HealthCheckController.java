package cheberiak.artem.bankingbackend.controller;

import io.reactivex.rxjava3.core.Single;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.Response;

@Path("health")
public class HealthCheckController {
    @GET
    public void healthCheck(@Suspended AsyncResponse ar) {
        Single.just(Response.ok().build()).subscribe(ar::resume).dispose();
    }
}
