package cheberiak.artem.bankingbackend.controller;

import cheberiak.artem.bankingbackend.controller.dto.response.ExceptionResponse;

import javax.validation.ConstraintViolationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import java.util.stream.Collectors;

@Provider
public class ConstraintViolationExceptionMapper implements ExceptionMapper<ConstraintViolationException> {
    @Override
    public Response toResponse(ConstraintViolationException e) {
        return Response
                .status(Response.Status.BAD_REQUEST)
                .type(MediaType.APPLICATION_JSON)
                .entity(new ExceptionResponse(e.getClass().getCanonicalName(),
                                              e.getConstraintViolations()
                                               .stream()
                                               .map(cv -> cv.getPropertyPath().toString() +
                                                          ": " +
                                                          cv.getMessage())
                                               .collect(Collectors.joining(System.lineSeparator()))))
                .build();
    }
}
