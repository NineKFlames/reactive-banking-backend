package cheberiak.artem.bankingbackend.controller.dto.response;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonSerialize
public class ExceptionResponse {
    private String className;
    private String message;

    public ExceptionResponse(Throwable err) {
        className = err.getClass().getCanonicalName();
        message = err.getMessage();
    }
}
