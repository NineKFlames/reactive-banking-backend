package cheberiak.artem.bankingbackend.controller.dto.request;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.math.BigDecimal;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonDeserialize
public class TransferFundsRequest {
    @NotEmpty(message = "Source account ID must not be null")
    private String source;

    @NotEmpty(message = "Target account ID must not be null")
    private String target;

    @Positive(message = "Invalid transfer amount: must be above zero")
    @NotNull(message = "Invalid transfer amount: null not allowed")
    @Digits(integer = 50,
            fraction = 2,
            message = "Invalid transfer amount: max 2 digits after decimal point; max 50 digits before decimal point")
    private BigDecimal amount;
}
