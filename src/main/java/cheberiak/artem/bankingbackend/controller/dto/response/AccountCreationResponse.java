package cheberiak.artem.bankingbackend.controller.dto.response;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonSerialize
public class AccountCreationResponse {
    private String accountId;
}
