package cheberiak.artem.bankingbackend.service;

import cheberiak.artem.bankingbackend.repository.UglyRepositoryWrapper;
import io.reactivex.rxjava3.core.Completable;
import io.reactivex.rxjava3.core.Single;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.UUID;

public class AccountService {
    private static class InstanceHolder {
        private static final AccountService INSTANCE = new AccountService();
    }

    private final UglyRepositoryWrapper repo;

    private AccountService() {
        repo = new UglyRepositoryWrapper();
    }

    public static AccountService getInstance() {
        return InstanceHolder.INSTANCE;
    }

    public Single<String> createNewAccount() {
        String newAccountId = generateNewAccountId();
        try {
            repo.createAccountSynchronously(newAccountId, 0L);
            return Single.just(newAccountId);
        } catch (Exception e) {
            return Single.error(e);
        }
    }

    private String generateNewAccountId() {
        return UUID.randomUUID().toString();
    }

    public Single<BigDecimal> getBalance(String id) {
        try {
            return Single.just(BigDecimal.valueOf(repo.getBalanceSynchronously(id))
                                         .divide(BigDecimal.valueOf(100), 2, RoundingMode.HALF_EVEN));
        } catch (Exception e) {
            return Single.error(e);
        }
    }

    public Completable deposit(String id, BigDecimal depositAmount) {
        return Completable.create(sub -> {
            try {
                repo.depositSynchronously(id, depositAmount);
                sub.onComplete();
            } catch (Exception e) {
                sub.onError(e);
            }
        });
    }

    public Completable withdraw(String id, BigDecimal depositAmount) {
        return Completable.create(sub -> {
            try {
                repo.withdrawSynchronously(id, depositAmount);
                sub.onComplete();
            } catch (Exception e) {
                sub.onError(e);
            }
        });
    }

    public Completable transfer(String target, String source, BigDecimal amount) {
        return Completable.create(sub -> {
            try {
                repo.transferSynchronously(target, source, amount);
                sub.onComplete();
            } catch (Exception e) {
                sub.onError(e);
            }
        });
    }
}
