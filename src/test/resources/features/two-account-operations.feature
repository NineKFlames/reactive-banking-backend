Feature: Two account operations
  As an external subsystem, I want to manipulate two balances at a time on behalf of my user,
  so that I can move funds between accounts.

  Background:
    Given server is up and running on localhost

  Scenario: Transfer money
    Given a source account is already created
    And source account has 1000 units in deposit already
    And a target account is already created
    And target account has 500 units in deposit already
    When I prepare 100 units as transfer amount
    And I build a prepared transfer request POST body
    And I make a POST request to "account/transfer" endpoint with a prepared body
    Then I get a 204 response code
    And response body is empty
    And I can check source account's balance via REST API
    And source account's balance response contains amount of 900
    And I can check target account's balance via REST API
    And target account's balance response contains amount of 600

  Scenario Outline: Transfer incorrect amount
    Given a source account is already created
    And source account has 1000 units in deposit already
    And a target account is already created
    And target account has 500 units in deposit already
    When I prepare <buggyAmount> units as transfer amount
    And I build a prepared transfer request POST body
    And I make a POST request to "account/transfer" endpoint with a prepared body
    Then I get a 400 response code
    And response body contains a valid exception response
    And exception message contains "<containedInMessage>"
    And I can check source account's balance via REST API
    And source account's balance response contains amount of 1000
    And I can check target account's balance via REST API
    And target account's balance response contains amount of 500
    Examples:
      | buggyAmount | containedInMessage                                        |
      | -100        | Invalid transfer amount: must be above zero               |
      | 0           | Invalid transfer amount: must be above zero               |
      | -3.141      | Invalid transfer amount: max 2 digits after decimal point |
      | 3.141       | Invalid transfer amount: max 2 digits after decimal point |
      | 10000       | Insufficient funds                                        |

  Scenario Outline: Transfer from incorrect account ID
    Given a target account is already created
    And target account has 500 units in deposit already
    When I prepare "<accountId>" as transfer source ID
    And I prepare 100 units as transfer amount
    And I build a prepared transfer request POST body
    And I make a POST request to "account/transfer" endpoint with a prepared body
    Then I get a 404 response code
    And response body contains a valid exception response
    And exception message contains "<containedInMessage>"
    And I can check target account's balance via REST API
    And target account's balance response contains amount of 500
    Examples:
      | accountId      | containedInMessage                       |
      | strange_string | Account not found for id: strange_string |
      | null           | Account not found for id: null           |

  Scenario Outline: Transfer to incorrect account ID
    Given a source account is already created
    And source account has 1000 units in deposit already
    When I prepare "<accountId>" as transfer target ID
    And I prepare 100 units as transfer amount
    And I build a prepared transfer request POST body
    And I make a POST request to "account/transfer" endpoint with a prepared body
    Then I get a 404 response code
    And response body contains a valid exception response
    And exception message contains "<containedInMessage>"
    And I can check source account's balance via REST API
    And source account's balance response contains amount of 1000
    Examples:
      | accountId      | containedInMessage                       |
      | strange_string | Account not found for id: strange_string |
      | null           | Account not found for id: null           |

  Scenario: Transfer to multiple accounts simultaneously
    Given a source account is already created
    And source account has 10000 units in deposit already
    And 3 target accounts are already created
    And all target accounts have 500 units in deposit each
    When I prepare 400 units as transfer amount
    And I prepare transfer request POST body for each target account
    And I make simultaneous asynchronous POST requests to "account/transfer" endpoint, one per prepared body
    Then I get a 204 response code for all of them
    And response body is empty for each response
    And I can check source account's balance via REST API
    And source account's balance response contains amount of 8800
    And I can check all target accounts' balances via REST API
    And each target account's balance response contains amount of 900

  Scenario: Transfer to multiple accounts simultaneously, but there's not enough for all
    Given a source account is already created
    And source account has 1000 units in deposit already
    And 3 target accounts are already created
    And all target accounts have 500 units in deposit each
    When I prepare 400 units as transfer amount
    And I prepare transfer request POST body for each target account
    And I make simultaneous asynchronous POST requests to "account/transfer" endpoint, one per prepared body
    Then I get a 204 response code for 2 of them
    And I get a 400 response code for 1 of them
    And response body is empty for 2 responses
    And response body contains a valid exception response for 1 response
    And exception message contains "Insufficient funds!" for all exception responses
    And I can check source account's balance via REST API
    And source account's balance response contains amount of 200
    And I can check all target accounts' balances via REST API
    And balance response for 2 of target accounts contains amount of 900
    And balance response for 1 of target accounts contains amount of 500

  Scenario: Transfer to a lot of accounts simultaneously, but there's not enough for all
    Given a source account is already created
    And source account has 1000 units in deposit already
    And 20 target accounts are already created
    And all target accounts have 500 units in deposit each
    When I prepare 400 units as transfer amount
    And I prepare transfer request POST body for each target account
    And I make simultaneous asynchronous POST requests to "account/transfer" endpoint, one per prepared body
    Then I get a 204 response code for 2 of them
    And I get a 400 response code for 18 of them
    And response body is empty for 2 responses
    And response body contains a valid exception response for 18 responses
    And exception message contains "Insufficient funds!" for all exception responses
    And I can check source account's balance via REST API
    And source account's balance response contains amount of 200
    And I can check all target accounts' balances via REST API
    And balance response for 2 of target accounts contains amount of 900
    And balance response for 18 of target accounts contains amount of 500