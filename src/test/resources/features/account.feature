Feature: Managing accounts
  As an external subsystem, I want to create an account on behalf of my user,
  so that I can use it as a single point of truth for balance info.

  Background:
    Given server is up and running on localhost

  Scenario: Create an account
    When I make a POST request to "account/new" endpoint with empty body
    Then I get a 201 response code
    And response body contains a valid account creation response
    And account creation response contains a valid account ID

  Scenario: Check balance
    Given an account is already created
    When I prepare said account's ID as request path param
    And I make a GET request to "account/%s" endpoint, parametrized with prepared value
    Then I get a 200 response code
    And response body contains a valid balance response
    And balance response contains said account's ID
    And balance response contains amount of 0

  Scenario: Check balance for null ID
    When I prepare "null" as request path param
    And I make a GET request to "account/%s" endpoint, parametrized with prepared value
    Then I get a 404 response code
    And response body contains a valid exception response
    And exception message contains "Account not found for id: null"

  Scenario: Check balance for non-UUID ID
    When I prepare "non-UUID-id" as request path param
    And I make a GET request to "account/%s" endpoint, parametrized with prepared value
    Then I get a 404 response code
    And response body contains a valid exception response
    And exception message contains "Account not found for id: non-UUID-id"
