Feature: Single account operations
  As an external subsystem, I want to manipulate balance on behalf of my user,
  so that I can move funds into and outside of the service.

  Background:
    Given server is up and running on localhost

  Scenario: Deposit money
    Given an account is already created
    When I prepare said account's ID and 100 units as a deposit request POST body
    And I make a POST request to "account/deposit" endpoint with a prepared body
    Then I get a 204 response code
    And response body is empty
    And I can check said account's balance via REST API
    And balance response contains amount of 100

  Scenario Outline: Deposit incorrect amount
    Given an account is already created
    When I prepare said account's ID and <buggyAmount> units as a deposit request POST body
    And I make a POST request to "account/deposit" endpoint with a prepared body
    Then I get a 400 response code
    And response body contains a valid exception response
    And exception message contains "<containedInMessage>"
    And I can check said account's balance via REST API
    And balance response contains amount of 0
    Examples:
      | buggyAmount | containedInMessage                                       |
      | -100        | Invalid deposit amount: must be above zero               |
      | 0           | Invalid deposit amount: must be above zero               |
      | -3.141      | Invalid deposit amount: must be above zero               |
      | 3.141       | Invalid deposit amount: max 2 digits after decimal point |

  Scenario Outline: Deposit to incorrect account ID
    When I prepare account ID "<accountId>" and 100 units as a deposit request POST body
    And I make a POST request to "account/deposit" endpoint with a prepared body
    Then I get a 404 response code
    And response body contains a valid exception response
    And exception message contains "<containedInMessage>"
    Examples:
      | accountId      | containedInMessage                       |
      | strange_string | Account not found for id: strange_string |
      | null           | Account not found for id: null           |

  Scenario: Withdraw money
    Given an account is already created
    And said account already has 1000 units in deposit already
    When I prepare said account's ID and 100 units as a withdrawal request POST body
    And I make a POST request to "account/withdraw" endpoint with a prepared body
    Then I get a 204 response code
    And response body is empty
    And I can check said account's balance via REST API
    And balance response contains amount of 900

  Scenario Outline: Withdraw incorrect amount
    Given an account is already created
    And said account already has 1000 units in deposit already
    When I prepare said account's ID and <buggyAmount> units as a withdrawal request POST body
    And I make a POST request to "account/withdraw" endpoint with a prepared body
    Then I get a 400 response code
    And response body contains a valid exception response
    And exception message contains "<containedInMessage>"
    And I can check said account's balance via REST API
    And balance response contains amount of 1000
    Examples:
      | buggyAmount | containedInMessage                                          |
      | -100        | Invalid withdrawal amount: must be above zero               |
      | 0           | Invalid withdrawal amount: must be above zero               |
      | -3.141      | Invalid withdrawal amount: must be above zero               |
      | 3.141       | Invalid withdrawal amount: max 2 digits after decimal point |
      | 10000       | Insufficient funds                                          |

  Scenario Outline: Withdraw from incorrect account ID
    When I prepare account ID "<accountId>" and 100 units as a withdrawal request POST body
    And I make a POST request to "account/withdraw" endpoint with a prepared body
    Then I get a 404 response code
    And response body contains a valid exception response
    And exception message contains "<containedInMessage>"
    Examples:
      | accountId      | containedInMessage                       |
      | strange_string | Account not found for id: strange_string |
      | null           | Account not found for id: null           |
