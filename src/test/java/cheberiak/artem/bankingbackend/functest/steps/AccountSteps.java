package cheberiak.artem.bankingbackend.functest.steps;

import cheberiak.artem.bankingbackend.controller.dto.request.DepositFundsRequest;
import cheberiak.artem.bankingbackend.controller.dto.request.WithdrawFundsRequest;
import cheberiak.artem.bankingbackend.controller.dto.response.AccountCreationResponse;
import cheberiak.artem.bankingbackend.controller.dto.response.BalanceResponse;
import cheberiak.artem.bankingbackend.functest.HttpHelper;
import cheberiak.artem.bankingbackend.functest.world.World;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;
import java.util.UUID;

import static java.net.HttpURLConnection.*;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.*;

public class AccountSteps {
    private final HttpHelper httpHelper;
    private final World world;

    public AccountSteps() {
        httpHelper = HttpHelper.getInstance();
        world = World.getInstance();
    }

    @Given("^an account is already created$")
    public void iHaveAlreadyCreatedAnAccount() {
        Response response = httpHelper.postEmpty("account/new");

        if (response.getStatus() != HTTP_CREATED) {
            fail("Failed to create an account; further scenario execution is useless.");
        }

        world.setAccountIdUnderTest(response.readEntity(AccountCreationResponse.class).getAccountId());
    }

    @Given("^said account already has (-?[\\d.]+) units in deposit already$")
    public void iHaveAlreadyDepositedUnitsThere(BigDecimal amount) {
        Response response = httpHelper.post("account/deposit",
                                            Entity.json(new DepositFundsRequest(world.getAccountIdUnderTest(),
                                                                                amount)));
        if (response.getStatus() != HTTP_NO_CONTENT) {
            fail("Failed to deposit funds to an account; further scenario execution is useless.");
        }
    }

    @When("^I prepare said account's ID as request path param$")
    public void iPrepareMyAccountSIDAsRequestPathParam() {
        httpHelper.setPathParam(world.getAccountIdUnderTest());
    }

    @When("^I prepare said account's ID and (-?[\\d.]+) units as a deposit request POST body$")
    public void iPrepareMyAccountSIDAndUnitsAsADepositRequestPOSTBody(BigDecimal depositAmount) {
        httpHelper.setPostBody(Entity.json(new DepositFundsRequest(world.getAccountIdUnderTest(), depositAmount)));
    }

    @When("^I prepare account ID \"(.+)\" and (-?[\\d.]+) units as a deposit request POST body$")
    public void iPrepareAccountIDAndUnitsAsADepositRequestPOSTBody(String accountId, BigDecimal depositAmount) {
        httpHelper.setPostBody(Entity.json(new DepositFundsRequest(accountId, depositAmount)));
    }

    @When("^I prepare said account's ID and (-?[\\d.]+) units as a withdrawal request POST body$")
    public void iPrepareMyAccountSIDAndUnitsAsAWithdrawalRequestPOSTBody(BigDecimal withdrawalAmount) {
        httpHelper.setPostBody(Entity.json(new WithdrawFundsRequest(world.getAccountIdUnderTest(), withdrawalAmount)));
    }

    @When("^I prepare account ID \"(.+)\" and (-?[\\d.]+) units as a withdrawal request POST body$")
    public void iPrepareAccountIDAndUnitsAsAWithdrawalRequestPOSTBody(String accountId, BigDecimal withdrawalAmount) {
        httpHelper.setPostBody(Entity.json(new WithdrawFundsRequest(accountId, withdrawalAmount)));
    }

    @Then("^account creation response contains a valid account ID$")
    public void responseBodyContainsAValidAccountCreationResponse() {
        String accountId = world.getAccountCreationResponse().getAccountId();
        assertNotNull(accountId);
        assertThat(accountId, not(equalTo(new UUID(0L, 0L).toString())));
    }

    @Then("^balance response contains said account's ID$")
    public void balanceResponseContainsMyAccountID() {
        assertThat(world.getBalanceResponse().getAccountId(), equalTo(world.getAccountIdUnderTest()));
    }

    @Then("^balance response contains amount of (-?[\\d.]+)$")
    public void balanceResponseContainsAmountOf(BigDecimal expectedBalance) {
        assertThat(world.getBalanceResponse().getBalance().compareTo(expectedBalance), equalTo(0));
    }

    @Then("^I can check said account's balance via REST API$")
    public void iCanCheckMyBalanceViaRESTAPI() {
        Response response = httpHelper.get("account/".concat(world.getAccountIdUnderTest()));
        assertThat(response.getStatus(), equalTo(HTTP_OK));
        world.setBalanceResponse(response.readEntity(BalanceResponse.class));
    }
}
