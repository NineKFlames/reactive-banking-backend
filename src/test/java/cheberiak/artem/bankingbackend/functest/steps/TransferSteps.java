package cheberiak.artem.bankingbackend.functest.steps;

import cheberiak.artem.bankingbackend.controller.dto.request.DepositFundsRequest;
import cheberiak.artem.bankingbackend.controller.dto.request.TransferFundsRequest;
import cheberiak.artem.bankingbackend.controller.dto.response.AccountCreationResponse;
import cheberiak.artem.bankingbackend.controller.dto.response.BalanceResponse;
import cheberiak.artem.bankingbackend.functest.HttpHelper;
import cheberiak.artem.bankingbackend.functest.world.Transfer;
import cucumber.api.java.After;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static java.net.HttpURLConnection.*;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.everyItem;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

public class TransferSteps {
    private final HttpHelper httpHelper;
    private final Transfer transfer;

    public TransferSteps() {
        httpHelper = HttpHelper.getInstance();
        transfer = Transfer.getInstance();
    }

    @After
    public void tearDown() {
        transfer.cleanUp();
    }

    @Given("^a source account is already created$")
    public void sourceAccountAlreadyCreated() {
        transfer.setSourceAccountIdUnderTest(
                createAccount("Failed to create target account; further scenario execution is useless.")
                        .readEntity(AccountCreationResponse.class).getAccountId());
    }

    @Given("^a target account is already created$")
    public void targetAccountAlreadyCreated() {
        transfer.setTargetAccountIdUnderTest(
                createAccount("Failed to create target account; further scenario execution is useless.")
                        .readEntity(AccountCreationResponse.class).getAccountId());
    }

    @Given("^(\\d+) target accounts? are already created$")
    public void multipleTargetAccountAlreadyCreated(int accountsNumber) {
        IntStream.range(0, accountsNumber)
                 .forEach(accountIndex -> transfer.addAsyncTargetAccountId(
                         createAccount("Failed to create target account number " +
                                       accountIndex +
                                       "; further scenario execution is useless.")
                                 .readEntity(AccountCreationResponse.class).getAccountId()));
    }

    private Response createAccount(String messageOnFailure) {
        Response response = httpHelper.postEmpty("account/new");

        if (response.getStatus() != HTTP_CREATED) {
            fail(messageOnFailure);
        }
        return response;
    }

    @Given("^source account has (-?[\\d.]+) units in deposit already$")
    public void sourceAccountAlreadyHasDeposit(BigDecimal amount) {
        depositStartingBalance(amount,
                               transfer.getSourceAccountIdUnderTest(),
                               "Failed to deposit funds to source account; further scenario execution is useless.");
    }

    @Given("^target account has (-?[\\d.]+) units in deposit already$")
    public void targetAccountAlreadyHasDeposit(BigDecimal amount) {
        depositStartingBalance(amount,
                               transfer.getTargetAccountIdUnderTest(),
                               "Failed to deposit funds to target account; further scenario execution is useless.");
    }

    @Given("^all target accounts have (-?[\\d.]+) units in deposit each$")
    public void targetAccountsHaveUnitsInDepositEach(BigDecimal startingBalance) {
        transfer.getMultipleTargetAccountIdsUnderTest()
                .forEach(targetId -> depositStartingBalance(startingBalance,
                                                            targetId,
                                                            "Failed to deposit funds to target account with id" +
                                                            targetId +
                                                            "; further scenario execution is useless."));
    }

    private void depositStartingBalance(BigDecimal startingBalance, String accountId, String s) {
        Response response = httpHelper.post("account/deposit",
                                            Entity.json(new DepositFundsRequest(accountId, startingBalance)));
        if (response.getStatus() != HTTP_NO_CONTENT) {
            fail(s);
        }
    }

    @When("^I prepare \"(.+)\" as transfer source ID$")
    public void iPrepareAsSourceAccountID(String sourceAccountId) {
        transfer.setSourceAccountIdUnderTest(sourceAccountId);
    }

    @When("^I prepare \"(.+)\" as transfer target ID$")
    public void iPrepareAsTargetAccountID(String targetAccountId) {
        transfer.setTargetAccountIdUnderTest(targetAccountId);
    }

    @When("^I prepare (.+) units as transfer amount$")
    public void iPrepareTransferAmount(BigDecimal depositAmount) {
        transfer.setTransferAmount(depositAmount);
    }

    @When("^I build a prepared transfer request POST body$")
    public void buildTransferEntity() {
        String targetAccountIdUnderTest = transfer.getTargetAccountIdUnderTest();
        httpHelper.setPostBody(getTransferRequestEntity(targetAccountIdUnderTest));
    }

    private Entity<TransferFundsRequest> getTransferRequestEntity(String targetAccountIdUnderTest) {
        return Entity.json(new TransferFundsRequest(transfer.getSourceAccountIdUnderTest(),
                                                    targetAccountIdUnderTest,
                                                    transfer.getTransferAmount()));
    }

    @When("^I prepare transfer request POST body for each target account$")
    public void iBuildAPreparedTransferRequestPOSTBodyForEachTargetAccount() {
        transfer.getMultipleTargetAccountIdsUnderTest()
                .stream()
                .map(this::getTransferRequestEntity).forEach(httpHelper::addAsyncPostBody);
    }

    @Then("^I can check source account's balance via REST API$")
    public void iCanCheckSourceBalanceViaRESTAPI() {
        transfer.setSourceBalanceResponse(checkBalanceForAccount(transfer.getSourceAccountIdUnderTest()));
    }

    @Then("^I can check target account's balance via REST API$")
    public void iCanCheckTargetBalanceViaRESTAPI() {
        String id = transfer.getTargetAccountIdUnderTest();
        transfer.setTargetBalanceResponse(checkBalanceForAccount(id));
    }

    public BalanceResponse checkBalanceForAccount(String id) {
        Response response = httpHelper.get("account/".concat(id));
        assertThat("Can't fetch balance for id " + id + "; further scenario execution is useless.",
                   response.getStatus(),
                   equalTo(HTTP_OK));
        return response.readEntity(BalanceResponse.class);
    }

    @Then("^I can check all target accounts' balances via REST API$")
    public void iCanCheckTargetAccountSBalancesViaRESTAPI() {
        transfer.getMultipleTargetAccountIdsUnderTest()
                .stream()
                .map(this::checkBalanceForAccount)
                .forEach(transfer::addAsyncTargetBalance);
    }

    @Then("^source account's balance response contains amount of (-?[\\d.]+)$")
    public void sourceBalanceResponseContainsAmountOf(BigDecimal expectedBalance) {
        assertThat(transfer.getSourceBalanceResponse().getBalance().compareTo(expectedBalance), equalTo(0));
    }

    @Then("^target account's balance response contains amount of (-?[\\d.]+)$")
    public void targetBalanceResponseContainsAmountOf(BigDecimal expectedBalance) {
        assertThat(transfer.getTargetBalanceResponse().getBalance().compareTo(expectedBalance), equalTo(0));
    }

    @Then("^each target account's balance response contains amount of (\\d+)$")
    public void eachTargetAccountSBalanceResponseContainsAmountOf(BigDecimal expectedBalance) {
        assertThat(transfer.getMultipleTargetBalanceResponses()
                           .stream()
                           .map(r -> r.getBalance().compareTo(expectedBalance))
                           .collect(Collectors.toList()),
                   everyItem(equalTo(0)));
    }

    @Then("^balance response for (\\d+) of target accounts contains amount of (-?[\\d.]+)$")
    public void balanceResponseForOfTargetAccountsContainsAmountOf(long expectedResponsesAmount,
                                                                   BigDecimal expectedBalance) {
        assertThat(transfer.getMultipleTargetBalanceResponses()
                           .stream()
                           .filter(r -> r.getBalance().compareTo(expectedBalance) == 0)
                           .count(),
                   equalTo(expectedResponsesAmount));
    }
}
