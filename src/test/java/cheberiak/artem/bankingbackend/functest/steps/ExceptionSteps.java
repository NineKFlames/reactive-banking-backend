package cheberiak.artem.bankingbackend.functest.steps;

import cheberiak.artem.bankingbackend.controller.dto.response.ExceptionResponse;
import cheberiak.artem.bankingbackend.functest.HttpHelper;
import cheberiak.artem.bankingbackend.functest.world.World;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import java.util.stream.Collectors;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.everyItem;
import static org.junit.Assert.assertThat;

public class ExceptionSteps {
    private final HttpHelper httpHelper;
    private final World world;

    public ExceptionSteps() {
        httpHelper = HttpHelper.getInstance();
        world = World.getInstance();
    }

    @When("^I prepare \"(.+)\" as request path param$")
    public void iPrepareANullAsRequestPathParam(String string) {
        httpHelper.setPathParam(string);
    }

    @Then("^exception message contains \"(.+)\"$")
    public void exceptionMessageContains(String message) {
        assertThat(world.getExceptionResponse().getMessage(), containsString(message));
    }

    @Then("^exception message contains \"(.+)\" for all exception responses$")
    public void exceptionMessageContainsForAllExceptionResponses(String message) {
        assertThat(world.getAsyncExceptionResponses()
                        .stream()
                        .map(ExceptionResponse::getMessage)
                        .collect(Collectors.toList()),
                   everyItem(containsString(message)));
    }
}
