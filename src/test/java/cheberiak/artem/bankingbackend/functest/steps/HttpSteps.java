package cheberiak.artem.bankingbackend.functest.steps;

import cheberiak.artem.bankingbackend.controller.dto.response.AccountCreationResponse;
import cheberiak.artem.bankingbackend.controller.dto.response.BalanceResponse;
import cheberiak.artem.bankingbackend.controller.dto.response.ExceptionResponse;
import cheberiak.artem.bankingbackend.functest.HttpHelper;
import cheberiak.artem.bankingbackend.functest.world.World;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.exc.MismatchedInputException;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import static java.net.HttpURLConnection.HTTP_OK;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

public class HttpSteps {
    private final ObjectMapper mapper;
    private final HttpHelper httpHelper;
    private final World world;

    public HttpSteps() {
        mapper = new ObjectMapper();
        httpHelper = HttpHelper.getInstance();
        world = World.getInstance();
    }

    @Before
    public void setUp() throws Exception {
        // JerseyTest has these annotated with JUnit annotations. which won't
        // fire with Cucumber, so need to call those by hand here
        httpHelper.setUp();
    }

    @After
    public void tearDown() throws Exception {
        world.cleanUp();
        // JerseyTest has these annotated with JUnit annotations. which won't
        // fire with Cucumber, so need to call those by hand here
        httpHelper.tearDown();
    }

    @Given("^server is up and running on localhost$")
    public void serverIsUpAndRunning() {
        String endpoint = "health";
        assertThat(httpHelper.get(endpoint).getStatus(), equalTo(HTTP_OK));
    }

    @When("^I make a POST request to \"(.+)\" endpoint with empty body$")
    public void makeAPOSTRequestToWithEmptyBody(String endpoint) {
        world.storeResponseDetails(httpHelper.postEmpty(endpoint));
    }

    @When("^I make a GET request to \"([^\"]*)\" endpoint, parametrized with prepared value$")
    public void makeAGETRequestToEndpointParametrizedWithPreparedValue(String endpointTemplate) {
        world.storeResponseDetails(httpHelper.getWithPreparedPathParam(endpointTemplate));
    }

    @When("^I make a POST request to \"(.+)\" endpoint with a prepared body$")
    public void iMakeAPOSTRequestToEndpointWithAPreparedBody(String endpoint) {
        world.storeResponseDetails(httpHelper.postPrepared(endpoint));
    }

    @When("^I make simultaneous asynchronous POST requests to \"(.+)\" endpoint, one per prepared body$")
    public void iMakeSimultaneousAsynchronousPOSTRequestsToEndpointOnePerPreparedBody(String endpoint)
            throws ExecutionException, InterruptedException {
        httpHelper.postAllPreparedBodiesAsynchronouslyTo(endpoint).forEach(world::storeAsyncResponseDetails);
    }

    @Then("^I get a (\\d+) response code$")
    public void iGetAResponseCode(int expectedStatus) {
        assertThat(world.getResponseStatus(), equalTo(expectedStatus));
    }

    @Then("^I get a (\\d+) response code for all of them$")
    public void iGetAResponseCodeForAllOfThem(int expectedStatus) {
        assertThat(world.getAsyncResponseStatuses(), everyItem(equalTo(expectedStatus)));
    }

    @Then("^I get a (\\d+) response code for (\\d+) of them$")
    public void iGetAResponseCodeForNOfThem(int expectedStatus, long expectedAmount) {
        assertThat(world.getAsyncResponseStatuses()
                        .stream()
                        .filter(s -> s == expectedStatus)
                        .count(),
                   equalTo(expectedAmount));
    }

    @Then("^response body contains a valid account creation response$")
    public void responseBodyContainsAValidAccountCreationResponse() throws IOException {
        // success if parsing hasn't thrown an exception
        world.setAccountCreationResponse(mapper.readValue(world.getResponseBody(), AccountCreationResponse.class));
    }

    @Then("^response body contains a valid balance response$")
    public void responseBodyContainsAValidBalanceResponse() throws IOException {
        // success if parsing hasn't thrown an exception
        world.setBalanceResponse(mapper.readValue(world.getResponseBody(), BalanceResponse.class));
    }

    @Then("^response body contains a valid exception response$")
    public void responseBodyContainsAValidExceptionResponse() throws IOException {
        // success if parsing hasn't thrown an exception
        world.setExceptionResponse(mapper.readValue(world.getResponseBody(), ExceptionResponse.class));
    }

    @Then("^response body contains a valid exception response for (\\d+) responses?$")
    public void responseBodyContainsAValidExceptionResponseForResponse(int expectedAmount) throws IOException {
        List<ExceptionResponse> responses = new ArrayList<>();

        for (String body : world.getAsyncResponseBodies()) {
            try {
                responses.add(mapper.readValue(body, ExceptionResponse.class));
            } catch (MismatchedInputException ignore) {
            }
        }

        assertThat(responses.size(), equalTo(expectedAmount));
        world.setAsyncExceptionResponses(responses);
    }

    @Then("^response body is empty$")
    public void responseBodyIsEmpty() {
        assertThat(world.getResponseBody(), isEmptyString());
    }

    @Then("^response body is empty for each response$")
    public void responseBodyIsEmptyForEachResponse() {
        assertThat(world.getAsyncResponseBodies(), everyItem(isEmptyString()));
    }

    @Then("^response body is empty for (\\d+) responses?$")
    public void responseBodyIsEmptyForResponses(long amount) {
        assertThat(world.getAsyncResponseBodies().stream().filter(String::isEmpty).count(), equalTo(amount));
    }
}
