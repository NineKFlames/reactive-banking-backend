package cheberiak.artem.bankingbackend.functest.world;

import cheberiak.artem.bankingbackend.controller.dto.response.BalanceResponse;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class Transfer {
    private BalanceResponse sourceBalanceResponse;
    private BalanceResponse targetBalanceResponse;
    private List<BalanceResponse> multipleTargetBalanceResponses = new ArrayList<>();
    private String sourceAccountIdUnderTest;
    private String targetAccountIdUnderTest;
    private List<String> multipleTargetAccountIdsUnderTest = new ArrayList<>();
    private BigDecimal transferAmount;

    private static class InstanceHolder {
        private static final Transfer INSTANCE = new Transfer();
    }

    private Transfer() {
    }

    public static Transfer getInstance() {
        return Transfer.InstanceHolder.INSTANCE;
    }

    public void cleanUp() {
        sourceBalanceResponse = null;
        targetBalanceResponse = null;
        multipleTargetBalanceResponses.clear();
        sourceAccountIdUnderTest = null;
        targetAccountIdUnderTest = null;
        multipleTargetAccountIdsUnderTest.clear();
        transferAmount = null;
    }

    public void addAsyncTargetAccountId(String id) {
        multipleTargetAccountIdsUnderTest.add(id);
    }

    public void addAsyncTargetBalance(BalanceResponse response) {
        multipleTargetBalanceResponses.add(response);
    }
}
