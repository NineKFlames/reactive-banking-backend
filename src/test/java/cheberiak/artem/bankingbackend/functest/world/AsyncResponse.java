package cheberiak.artem.bankingbackend.functest.world;

import lombok.Value;

import javax.ws.rs.core.Response;

@Value
public class AsyncResponse {
    private final String body;
    private final int status;
    private final Response originalResponse;

    public static AsyncResponse fromResponse(Response response) {
        return new AsyncResponse(response.readEntity(String.class), response.getStatus(), response);
    }
}
