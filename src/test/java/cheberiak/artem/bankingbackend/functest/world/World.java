package cheberiak.artem.bankingbackend.functest.world;

import cheberiak.artem.bankingbackend.controller.dto.response.AccountCreationResponse;
import cheberiak.artem.bankingbackend.controller.dto.response.BalanceResponse;
import cheberiak.artem.bankingbackend.controller.dto.response.ExceptionResponse;
import lombok.Getter;
import lombok.Setter;

import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
public class World {
    private String responseBody;
    private Integer responseStatus;
    private List<AsyncResponse> asyncResponses = new ArrayList<>();
    private AccountCreationResponse accountCreationResponse;
    private BalanceResponse balanceResponse;
    private ExceptionResponse exceptionResponse;
    private List<ExceptionResponse> asyncExceptionResponses;
    private String accountIdUnderTest;

    private static class InstanceHolder {
        private static final World INSTANCE = new World();

    }
    private World() {
    }

    public static World getInstance() {
        return InstanceHolder.INSTANCE;
    }

    public void cleanUp() {
        responseBody = null;
        responseStatus = null;
        asyncResponses.clear();
        accountCreationResponse = null;
        balanceResponse = null;
        accountIdUnderTest = null;
        exceptionResponse = null;
    }

    public void storeResponseDetails(Response response) {
        responseStatus = response.getStatus();
        responseBody = response.readEntity(String.class);
    }

    public List<Integer> getAsyncResponseStatuses() {
        return asyncResponses.stream().map(AsyncResponse::getStatus).collect(Collectors.toList());
    }

    public List<String> getAsyncResponseBodies() {
        return asyncResponses.stream().map(AsyncResponse::getBody).collect(Collectors.toList());
    }

    public void storeAsyncResponseDetails(Response response) {
        asyncResponses.add(AsyncResponse.fromResponse(response));
    }
}
