package cheberiak.artem.bankingbackend.functest;

import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(features = "src/test/resources/features", glue = "cheberiak.artem.bankingbackend.functest.steps")
public class CucumberRunnetIT {}
