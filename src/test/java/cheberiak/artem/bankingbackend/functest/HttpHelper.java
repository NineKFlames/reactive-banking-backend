package cheberiak.artem.bankingbackend.functest;

import cheberiak.artem.bankingbackend.config.Config;
import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import org.glassfish.jersey.test.JerseyTest;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

@Getter
@Setter
public class HttpHelper extends JerseyTest {
    private String pathParam;
    private Entity<?> postBody;
    private List<Entity<?>> asyncPostBodies = new ArrayList<>();

    private static class InstanceHolder {
        private static final HttpHelper INSTANCE = new HttpHelper();
    }

    private HttpHelper() {
        super();
    }

    public static HttpHelper getInstance() {
        return InstanceHolder.INSTANCE;
    }

    @Override
    public Application configure() {
        return new Config();
    }

    @Override
    public void tearDown() throws Exception {
        cleanUp();
        super.tearDown();
    }

    public Response post(String endpoint, Entity<?> entity) {
        return target(endpoint).request().post(entity);
    }

    public Response postPrepared(String endpoint) {
        return post(endpoint, postBody);
    }

    public Response postEmpty(String endpoint) {
        return post(endpoint, null);
    }

    public Response get(String endpoint) {
        return target(endpoint).request().get();
    }

    public Response getWithPreparedPathParam(String endpointTemplate) {
        return get(String.format(endpointTemplate, pathParam));
    }

    public List<Response> postAllPreparedBodiesAsynchronouslyTo(String endpoint)
            throws ExecutionException, InterruptedException {
        List<Future<Response>> futureResponses = asyncPostBodies.stream()
                                                                .parallel()
                                                                .map(e -> target(endpoint).request().async().post(e))
                                                                .collect(Collectors.toList());
        List<Response> returnValue = new ArrayList<>();

        // future.get throws checked exceptions, so can't use it in streams
        for (var future : futureResponses) {
            returnValue.add(future.get());
        }

        return returnValue;
    }

    public void addAsyncPostBody(Entity<?> body) {
        asyncPostBodies.add(body);
    }

    public void cleanUp() {
        pathParam = null;
        asyncPostBodies.clear();
    }
}
