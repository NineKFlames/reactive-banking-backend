#!/bin/bash

set -e

SCRIPT_DIR="$(readlink -f $(dirname $0))"
mkdir -p "$SCRIPT_DIR/repo"
mvn deploy:deploy-file \
    -Dfile=src/main/resources/lib/task-tools-1.0-all.jar \
    -DgroupId=com.intel471 \
    -DartifactId=task-tools \
    -Dpackaging=jar \
    -Dversion=1.0 \
    -Durl="file://${SCRIPT_DIR}/repo"
